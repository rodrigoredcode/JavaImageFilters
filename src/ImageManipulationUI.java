import javax.management.InvalidAttributeValueException;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class ImageManipulationUI {
	
	private JFrame mainFrame;
	private JPanel controlPanel;
	private JFileChooser chooser;
	private JLabel label1;
	private ImageReader imgR;
	private ImagePGM imgPGM;
	
	public static void main(String[] args) {
		new ImageManipulationUI().buildWindow();

	}
	private void buildWindow() {
		prepareWindow();
		prepareControlPanel();
		prepareLabel1();
		prepareBtnChooser();
		prepareBtnUncover();
		showWindow();
		//prepareLabel2();
		//prepareRdioBtns();
	}
	private void prepareWindow() {
		mainFrame = new JFrame("Image Manipulation");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	private void prepareControlPanel() {
		controlPanel = new JPanel();
		mainFrame.add(controlPanel);
	}
	private void prepareChooser(){
		chooser = new JFileChooser();
	    chooser.setFileFilter(new FileNameExtensionFilter("PGM & PPM Images", "pgm", "ppm"));
	    chooser.setAcceptAllFileFilterUsed(false);
	    chooser.setDialogTitle("Select an image");
	    chooser.showOpenDialog(null);
	    
	}
	private void prepareBtnChooser() {
		JButton btnChooser = new JButton("...");
		btnChooser.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	prepareChooser();
		    }
		  });
		controlPanel.add(btnChooser);
	}
	private void prepareBtnUncover(){
		JButton btnUncover = new JButton("Uncover Message");
		btnUncover.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
				try {
					File file = new File(chooser.getCurrentDirectory() + "/" + chooser.getDescription(chooser.getSelectedFile()));
					FileInputStream fileIs = new FileInputStream(file);
					imgR = new ImageReader();
					imgR.read(fileIs);
					imgPGM = new ImagePGM();
					imgPGM.uncoverMessage(fileIs, imgR.getHideMsgPosition());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvalidAttributeValueException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
		  });
		controlPanel.add(btnUncover);
	}
//	private void readImage() {
//		try{
//			FileInputStream fileIs = new FileInputStream(chooser.getSelectedFile());
//			imgR.read(fileIs);
//			imgBefore = new BufferedImage(imgR.getWidth(), imgR.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
//			byte[] pixels = ((DataBufferByte) imgBefore.getRaster().getDataBuffer()).getData();
//			for(int i=0; i<(imgR.getWidth()*imgR.getHeight()); i++){
//				pixels[i] = imgR.getPixels(i);
//			}
//		}catch(FileNotFoundException e){
//			System.err.print("File not found");
//		}catch(InvalidAttributeValueException e){
//			System.err.print("Image in wrong format");
//		}catch(IOException e){
//			System.err.print("Impossible to read this bytes");
//		}
//	}
	private void prepareLabel1() {
		label1 = new JLabel();
		label1.setSize(300, 200);
		label1.setText("Choose an PGM image");
		controlPanel.add(label1);
	}
	private void showWindow() {
		mainFrame.pack();
		mainFrame.setSize(600, 600);
		mainFrame.setVisible(true);
	}
	
	
}
