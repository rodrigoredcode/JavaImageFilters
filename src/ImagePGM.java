import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JOptionPane;


public class ImagePGM extends Image {
	private String message = "";
	
	public String getString(){
		return message;
	}
	public void uncoverMessage(FileInputStream fileIs, int messagePos) throws IOException{
		byte input;
		int lsb;
		byte msgChar = 0;
		for(int i=0; i<messagePos; i++){
			fileIs.read();
		}
		while((char)msgChar != '#'){
			msgChar =0;
			for(int i=0; i<8; i++){
				input = (byte)fileIs.read();
				lsb = (input & 0x1);
				if(i==0){
					msgChar = (byte)(msgChar | (lsb << 1));
				}else if(i==7){
					msgChar = (byte)(msgChar | lsb);
				}else{
					msgChar = (byte)((msgChar << 1) | (lsb << 1));
				}
			}
			message += (char)msgChar;
		}
		JOptionPane msg = new JOptionPane();
		msg.showMessageDialog(null, "The message is: " + message);
	}
	public void filterApply(){
		
	}
}
