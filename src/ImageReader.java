import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.management.InvalidAttributeValueException;


public class ImageReader {
	private String type;
	private String comentary = "";
	private int hideMsgPosition;
	private int width;
	private int height;
	private int maxValPixel;
	//private byte[] pixels;
	
	public String getType() {
		return type;
	}
	
	public String getComentary() {
		return comentary;
	}
	
	public int getHideMsgPosition() {
		return hideMsgPosition;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getMaxValPixel() {
		return maxValPixel;
	}
//	public byte getPixels(int position){
//		return pixels[position];
//	}
	private String readLine(FileInputStream fileIs){
		String line= "";
		byte byteIn;
		try{
			while((byteIn = (byte)fileIs.read()) != '\n'){
				line += (char)byteIn;
			}
		}catch(IOException e){
			System.err.print("Impossible to read this bytes");
		}
		return line;
	}
	
	public void read(FileInputStream fileIs) throws InvalidAttributeValueException, IOException{
		byte bb;
		String line = readLine(fileIs);
		if("P5".equals(line) || "P6".equals(line)){
			type = line;
			line = readLine(fileIs);
			int count = 0;
			while(line.startsWith("#")){
				comentary += line;
				if(count == 0){
					Scanner tempSc = new Scanner(comentary);
					tempSc.next();
					hideMsgPosition = tempSc.nextInt();
					tempSc.close();
				}
				line = readLine(fileIs);
				count++;
			}
			Scanner passageSc = new Scanner(line);
			if(passageSc.hasNext() && passageSc.hasNextInt()){
				width = passageSc.nextInt();
			}
			if(passageSc.hasNext() && passageSc.hasNextInt()){
				height = passageSc.nextInt();
			}
			line = readLine(fileIs);
			passageSc.close();
			passageSc = new Scanner(line);
			maxValPixel = passageSc.nextInt();
			passageSc.close();
			
//			imgPGM = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
//			pixels = ((DataBufferByte) imgPGM.getRaster().getDataBuffer()).getData();
//			count =0;
//			while(count < (height*width)) {
//				//System.err.println((byte)fileIs.read());
//				bb = (byte) fileIs.read();
//				pixels[count] = bb;
//				count++;
//			}				
		}
	}
}
